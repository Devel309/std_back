package com.llumpa.stdllumpaback.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.llumpa.stdllumpaback.dto.ActContraseñaUsuarioDto;
import com.llumpa.stdllumpaback.dto.BuscarExpedienteDto;
import com.llumpa.stdllumpaback.dto.BuscarLoginDto;
import com.llumpa.stdllumpaback.dto.ListEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListLoginDto;
import com.llumpa.stdllumpaback.dto.PersonaDto;
import com.llumpa.stdllumpaback.services.LoginService;

@RestController
@RequestMapping("/apiLogin")
@CrossOrigin(origins = "*")
public class LoginController {

	@Autowired
	LoginService loginService;
	
	 @PostMapping("/listLoginUser")
		public ResponseEntity<?> listExpediente(@RequestBody BuscarLoginDto buscarLoginDto)
			{
				try {
					BuscarLoginDto  param = new BuscarLoginDto();
					param.setUser(buscarLoginDto.getUser()!=""?buscarLoginDto.getUser():"");
					param.setPassword(buscarLoginDto.getPassword()!=""?buscarLoginDto.getPassword():"");
					System.out.println("param: "+param);
					List<ListLoginDto> listResponse =  loginService.listLogin(param);
					return new ResponseEntity<>(listResponse, HttpStatus.OK);
				}catch (Exception e) {
					e.printStackTrace();
					return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
				}
			}
	 
		@PostMapping("/actualizarContraseña")
		public ResponseEntity<?> updatePassword(@RequestBody ActContraseñaUsuarioDto actContraseñaUsuarioDto) {
			System.out.println("controller");
			try {
				HashMap<Object, Object> response = new HashMap<>();
				response.put("idUsuario", actContraseñaUsuarioDto.getIdUsuario());
				response.put("password", actContraseñaUsuarioDto.getPassword());
				response.put("result", "");
				loginService.updatePassword(response);
				System.out.println("[insertProyecto respuesta]  " + response.get("respuesta"));
				Map<String, Object> res = new HashMap();
				res.put("result", response.get("respuesta"));
				return new ResponseEntity<>(res, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
			}
		}
}
