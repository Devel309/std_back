package com.llumpa.stdllumpaback.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.llumpa.stdllumpaback.dto.BuscarEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListPersonaDto;
import com.llumpa.stdllumpaback.dto.PersonaDto;
import com.llumpa.stdllumpaback.services.PersonaService;

@RestController
@RequestMapping("/apiPersona")
@CrossOrigin(origins = "*")
public class PersonaController {

	@Autowired
	PersonaService personaService;

	@PostMapping("/insertPersona")
	public ResponseEntity<?> insertPersona(@RequestBody PersonaDto personaDto) {
		System.out.println("controller");
		try {
			HashMap<Object, Object> response = new HashMap<>();
			response.put("dni", personaDto.getDni());
			response.put("apellidos", personaDto.getApellidos());
			response.put("nombres", personaDto.getNombres());
			response.put("ruc", personaDto.getRuc());
			response.put("razonSocial", personaDto.getRazonSocial());
			response.put("direccion", personaDto.getDireccion());
			response.put("fechNacimiento", personaDto.getFechNacimiento());
			response.put("result", "");
			personaService.createPersona(response);
			System.out.println("[insertProyecto respuesta]  " + response.get("respuesta"));
			Map<String, Object> res = new HashMap();
			res.put("result", response.get("respuesta"));
			return new ResponseEntity<>(res, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/listPersona")
	public ResponseEntity<?> listEmpleado(@RequestParam("dni") Integer dni) {
		try {
			System.out.println("dni" + dni);
			BuscarEmpleadoDto param = new BuscarEmpleadoDto();
			param.setDni(dni > 0 ? dni : null);
			System.out.println("dni: " + param);
			List<ListPersonaDto> listResponse = personaService.listPersona(param);
			return new ResponseEntity<>(listResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/actualizarPersona")
	public ResponseEntity<?> updatePersona(@RequestBody PersonaDto personaDto) {
		System.out.println("controller");
		try {
			HashMap<Object, Object> response = new HashMap<>();
			response.put("idPersona", personaDto.getIdPersona());
			response.put("dni", personaDto.getDni());
			response.put("apellidos", personaDto.getApellidos());
			response.put("nombres", personaDto.getNombres());
			response.put("ruc", personaDto.getRuc());
			response.put("razonSocial", personaDto.getRazonSocial());
			response.put("fechNacimiento", personaDto.getFechNacimiento());
			response.put("direccion", personaDto.getDireccion());
			response.put("result", "");
			personaService.updatePersona(response);
			System.out.println("[insertProyecto respuesta]  " + response.get("respuesta"));
			Map<String, Object> res = new HashMap();
			res.put("result", response.get("respuesta"));
			return new ResponseEntity<>(res, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
