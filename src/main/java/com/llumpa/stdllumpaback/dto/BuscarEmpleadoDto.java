package com.llumpa.stdllumpaback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuscarEmpleadoDto {
	public Integer dni;
	public String apNombres;
}	
