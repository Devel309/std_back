package com.llumpa.stdllumpaback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListComboAreaDto {
	private Integer id;
	private	String area;
	private	String descripcion;
}
