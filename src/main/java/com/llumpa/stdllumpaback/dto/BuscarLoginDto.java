package com.llumpa.stdllumpaback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuscarLoginDto {
	private String user;
	private String password;
}
