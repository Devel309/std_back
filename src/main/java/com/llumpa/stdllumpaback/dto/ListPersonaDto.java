package com.llumpa.stdllumpaback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListPersonaDto {
	
	private Integer idPersona;
	private Integer dni;
	private String apellidos;
	private String nombres;
	private String ruc;
	private String razonSocial;
	private String direccion;
	
	
}
