package com.llumpa.stdllumpaback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DerivarExpedienteDto {
	private String descripcion;
	private Integer idusuario;
	private Integer idExpediente;
	private Integer idArea;
	private String pdfExp;
	
	private Integer respuesta;
}
