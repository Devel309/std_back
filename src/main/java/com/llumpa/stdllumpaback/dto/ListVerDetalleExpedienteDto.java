package com.llumpa.stdllumpaback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListVerDetalleExpedienteDto {
	private String area;
	private String fecha;
	private String descripcion;
}
