package com.llumpa.stdllumpaback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListMotivoInstitucionDto {
	private Integer id;
	private String nombre;
	private String desRuc;
	
}
