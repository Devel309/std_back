package com.llumpa.stdllumpaback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActContraseñaUsuarioDto {
	private Integer idUsuario;
	private String password;
	
	private Integer respuesta;
}
