package com.llumpa.stdllumpaback.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioDto {
	private String usuario;
	private Integer respuesta;
}
