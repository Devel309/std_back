package com.llumpa.stdllumpaback.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.llumpa.stdllumpaback.dto.BuscarEmpleadoDto;
import com.llumpa.stdllumpaback.dto.BuscarExpedienteDto;
import com.llumpa.stdllumpaback.dto.ListComboAreaDto;
import com.llumpa.stdllumpaback.dto.ListEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListExpedienteDto;
import com.llumpa.stdllumpaback.dto.ListSeguimientoExternoDto;
import com.llumpa.stdllumpaback.dto.ListVerDetalleExpedienteDto;
import com.llumpa.stdllumpaback.mappers.ExpedienteMapper;
import com.llumpa.stdllumpaback.services.ExpedienteService;
import com.llumpa.stdllumpaback.util.Constant;

@Service("expedienteService")
public class ExpedienteServiceImpl implements ExpedienteService{
	
	@Autowired
	ExpedienteMapper expedienteMapper;
	
	@Override
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void createExpediente(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services12:  " +ltp);
		try {
			expedienteMapper.createExpediente(ltp);	
			System.out.println("ltp"+ltp);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	@Override
	public List<ListExpedienteDto> listExpediente(BuscarExpedienteDto param) throws Exception{
		try {
			Map responseMap = new HashMap<>();
			System.out.println("aqui: "+param.getIdTipoExp() + "\n"+param.getDni());
			responseMap.put("idTipoExp",param.getIdTipoExp());
			responseMap.put("correlativo",param.getCorrelativo());
			responseMap.put("dni",param.getDni());
			responseMap.put("fechaInicio",param.getFechaInicio());
			responseMap.put("fechaFin",param.getFechaFin());
			responseMap.put("area",param.getArea());
			responseMap.put("idArea",param.getIdArea());
			return expedienteMapper.listExpediente(responseMap);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}

	@Override
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void derivarExpediente(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services:  " +ltp);
		try {
			expedienteMapper.derivarExpediente(ltp);	
			System.out.println("ltp"+ltp);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public List<ListVerDetalleExpedienteDto> listverDetalleExpedienteDto(Integer idExpediente) throws Exception {
		System.out.println("services12:  " +idExpediente);
		try {
			//System.out.println("44555"+expedienteMapper.listverDetalleExpedienteDto(ltp));
			return expedienteMapper.listverDetalleExpedienteDto(idExpediente);	
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public List<ListSeguimientoExternoDto> listSeguiExpExt(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services12:  " +ltp);
		try {
			//System.out.println("44555"+expedienteMapper.listverDetalleExpedienteDto(ltp));
			return expedienteMapper.listSeguiExpExt(ltp);	
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
	
}
