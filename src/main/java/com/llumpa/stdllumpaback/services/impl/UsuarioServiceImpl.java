package com.llumpa.stdllumpaback.services.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.llumpa.stdllumpaback.mappers.UsuarioMapper;
import com.llumpa.stdllumpaback.services.UsuarioService;
import com.llumpa.stdllumpaback.util.Constant;

@Service("serviceUsuario")
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	UsuarioMapper usuarioMapper;
	
	@Override
	@Transactional (propagation = Propagation.REQUIRED,isolation = Isolation.DEFAULT,timeout=36000, rollbackFor =Exception.class)
	public void listUsuario(HashMap<Object, Object> ltp) throws Exception {
		System.out.println("services12:  " +ltp);
		try {
			usuarioMapper.listUsuario(ltp);	
			System.out.println("ltp"+ltp);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(Constant.MSJ_ERROR);
		}
	}
}
