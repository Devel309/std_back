package com.llumpa.stdllumpaback.services;

import java.util.HashMap;
import java.util.List;

import com.llumpa.stdllumpaback.dto.ListCapacitacionDto;
import com.llumpa.stdllumpaback.dto.ListPapeletaDto;

public interface SalidaPyCService {

	public void createPapeleta(HashMap<Object, Object> ltp) throws Exception;

	public List<ListPapeletaDto> listPapeleta() throws Exception;

	public void createCapacitacion(HashMap<Object, Object> ltp) throws Exception;

	public List<ListCapacitacionDto> listCapacitacion() throws Exception;

}
