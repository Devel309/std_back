package com.llumpa.stdllumpaback.services;

import java.util.HashMap;
import java.util.List;

import com.llumpa.stdllumpaback.dto.BuscarEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListPersonaDto;


public interface PersonaService {

	public void createPersona(HashMap<Object, Object> ltp) throws Exception;

	public List<ListPersonaDto> listPersona(BuscarEmpleadoDto param) throws Exception;

	public void updatePersona(HashMap<Object, Object> ltp) throws Exception;

	
}
