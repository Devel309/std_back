package com.llumpa.stdllumpaback.services;

import java.util.HashMap;
import java.util.List;

import com.llumpa.stdllumpaback.dto.BuscarExpedienteDto;
import com.llumpa.stdllumpaback.dto.ListEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ReporteExpedientesDto;

public interface ReporteExpedienteService {

	public HashMap<String, Object> listReporte(BuscarExpedienteDto param) throws Exception;


}
