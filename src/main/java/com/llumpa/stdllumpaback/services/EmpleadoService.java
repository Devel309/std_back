package com.llumpa.stdllumpaback.services;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.llumpa.stdllumpaback.dto.BuscarEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListEmpleadoDto;



public interface EmpleadoService {

	public void createRiesgo(HashMap<Object, Object> ltp) throws Exception;

	public List<ListEmpleadoDto> listEmpleado(BuscarEmpleadoDto param) throws Exception;

	public void updateEmpleado(HashMap<Object, Object> ltp) throws Exception;

	
}
