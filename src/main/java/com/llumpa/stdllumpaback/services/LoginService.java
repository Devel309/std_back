package com.llumpa.stdllumpaback.services;

import java.util.HashMap;
import java.util.List;

import com.llumpa.stdllumpaback.dto.BuscarLoginDto;
import com.llumpa.stdllumpaback.dto.ListEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListLoginDto;

public interface LoginService {


	public List<ListLoginDto> listLogin(BuscarLoginDto param) throws Exception;

	public void updatePassword(HashMap<Object, Object> ltp) throws Exception;

}
