package com.llumpa.stdllumpaback.services;

import java.util.HashMap;
import java.util.List;

import com.llumpa.stdllumpaback.dto.BuscarExpedienteDto;
import com.llumpa.stdllumpaback.dto.ListComboAreaDto;
import com.llumpa.stdllumpaback.dto.ListEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListExpedienteDto;
import com.llumpa.stdllumpaback.dto.ListSeguimientoExternoDto;
import com.llumpa.stdllumpaback.dto.ListVerDetalleExpedienteDto;

public interface ExpedienteService {

	public void createExpediente(HashMap<Object, Object> ltp) throws Exception;

	public List<ListExpedienteDto> listExpediente(BuscarExpedienteDto param) throws Exception;

	public void derivarExpediente(HashMap<Object, Object> ltp) throws Exception;

	public List<ListVerDetalleExpedienteDto> listverDetalleExpedienteDto(Integer idExpediente) throws Exception;
	
	public List<ListSeguimientoExternoDto> listSeguiExpExt(HashMap<Object, Object> ltp) throws Exception;
}
