package com.llumpa.stdllumpaback.services;

import java.util.List;

import com.llumpa.stdllumpaback.dto.LisEmpleadoPersonaDto;
import com.llumpa.stdllumpaback.dto.ListComboAreaDto;
import com.llumpa.stdllumpaback.dto.ListComboCargoDto;
import com.llumpa.stdllumpaback.dto.ListMotivoInstitucionDto;

public interface ListComboService {

	public List<ListComboAreaDto> listComboArea() throws Exception;

	public List<ListComboCargoDto> listComboCargo() throws Exception;

	public List<ListMotivoInstitucionDto> listComboMotivo() throws Exception;

	public List<ListMotivoInstitucionDto> listComboTema() throws Exception;

	public List<ListMotivoInstitucionDto> listInstitucion(ListMotivoInstitucionDto param) throws Exception;

	public List<LisEmpleadoPersonaDto> lisEmpleadoPersona(ListMotivoInstitucionDto param) throws Exception;


}
