package com.llumpa.stdllumpaback.mappers;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.llumpa.stdllumpaback.dto.LisEmpleadoPersonaDto;
import com.llumpa.stdllumpaback.dto.ListComboAreaDto;
import com.llumpa.stdllumpaback.dto.ListComboCargoDto;
import com.llumpa.stdllumpaback.dto.ListMotivoInstitucionDto;

@Mapper
public interface ListComboMapper {

	public List<ListComboAreaDto> listComboArea(Map response) throws Exception;
	
	public List<ListComboCargoDto> listComboCargo(Map response) throws Exception;
	
	public List<ListMotivoInstitucionDto> listComboTema(Map response) throws Exception;
	
	public List<ListMotivoInstitucionDto> listComboMotivo(Map response) throws Exception;
	
	public List<ListMotivoInstitucionDto> listInstitucion(Map response) throws Exception;
	
	public List<LisEmpleadoPersonaDto> lisEmpleadoPersona(Map response) throws Exception;
	
	
}
