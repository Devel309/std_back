package com.llumpa.stdllumpaback.mappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.llumpa.stdllumpaback.dto.ListEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListExpedienteDto;
import com.llumpa.stdllumpaback.dto.ListSeguimientoExternoDto;
import com.llumpa.stdllumpaback.dto.ListVerDetalleExpedienteDto;

@Mapper
public interface ExpedienteMapper {

	public void createExpediente(HashMap<Object, Object> ltp);
	
	public List<ListExpedienteDto> listExpediente(Map response) throws Exception;
	
	public void derivarExpediente(HashMap<Object, Object> ltp);
	
	//public List<ListVerDetalleExpedienteDto> listverDetalleExpedienteDto(Map response) throws Exception;
	
	public List<ListSeguimientoExternoDto> listSeguiExpExt(Map response) throws Exception;

	public List<ListVerDetalleExpedienteDto> listverDetalleExpedienteDto(Integer idExpediente) throws Exception;

	
}
