package com.llumpa.stdllumpaback.mappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.llumpa.stdllumpaback.dto.ListEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListLoginDto;

@Mapper
public interface LoginMapper {
	
	public List<ListLoginDto> listLogin(Map response) throws Exception;
	
	public void updatePassword(HashMap<Object, Object> ltp);
}
