package com.llumpa.stdllumpaback.mappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.llumpa.stdllumpaback.dto.BuscarEmpleadoDto;
import com.llumpa.stdllumpaback.dto.ListEmpleadoDto;


@Mapper
public interface EmpleadoMapper {
	public void createEmpleado(HashMap<Object, Object> ltp);
	
	public List<ListEmpleadoDto> listEmpleado(Map response) throws Exception;
	
	public void updateEmpleado(HashMap<Object, Object> ltp);
}
